﻿using System;
using CraveRemoteControl.Web;
using Microsoft.Owin.Hosting;

namespace CraveRemoteControl.SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program();
        }

        public Program()
        {
            using (WebApp.Start<Startup>("http://*:1337"))
            {
                Console.ReadLine();
            }
        }
    }
}
