﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using CraveRemoteControl.Managers;
using CraveRemoteControl.Util;
using Microsoft.Owin;

namespace CraveRemoteControl.WebSockets
{
    public class WebSocketConnection
    {
        internal async Task AcceptRequestAsync(IOwinContext context)
        {
            var accept = context.Get<Action<IDictionary<string, object>, Func<IDictionary<string, object>, Task>>>("websocket.Accept");
            if (accept == null)
            {
                context.Response.StatusCode = 400;
                context.Response.Write("Ain't no websocket");
                return;
            }

            accept(null, this.ProcessRequestAsync);
        }

        private async Task ProcessRequestAsync(IDictionary<string, object> websocketContext)
        {
            WebSocket webSocket;

            WebSocketConnectionType connectionType = WebSocketConnectionType.Unknown;
            NameValueCollection queryParameters = null;

            object value;
            if (websocketContext.TryGetValue(typeof(WebSocketContext).FullName, out value))
            {
                WebSocketContext webSocketContext = (WebSocketContext)value;

                webSocket = webSocketContext.WebSocket;
                queryParameters = HttpUtility.ParseQueryString(webSocketContext.RequestUri.Query);

                if (webSocketContext.RequestUri.AbsolutePath.Contains("emenu"))
                {
                    connectionType = WebSocketConnectionType.Emenu;
                }
                else if (webSocketContext.RequestUri.AbsolutePath.Contains("view"))
                {
                    connectionType = WebSocketConnectionType.Viewer;
                }
                else if (webSocketContext.RequestUri.AbsolutePath.Contains("writelog"))
                {
                    connectionType = WebSocketConnectionType.WriteLog;
                }
                else if (webSocketContext.RequestUri.AbsolutePath.Contains("readlog"))
                {
                    connectionType = WebSocketConnectionType.ReadLog;
                }
            }
            else
            {
                webSocket = new WebSocketOwin(websocketContext);
            }

            bool authorized = Authorization.IsAuthorized(queryParameters);
            if (authorized && connectionType != WebSocketConnectionType.Unknown)
            {
                WebSocketClient client = new WebSocketClient();
                ConnectionManager.Instance.AddConnection(client, connectionType, queryParameters["identifier"]);

                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                try
                {
                    await client.ProcessWebSocketRequestAsync(webSocket, cancellationTokenSource.Token);
                }
                catch
                {
                    // This error was already handled by other layers
                    // we can no-op here so we don't cause an unobserved exception
                }

                // Always try to close async, if the websocket already closed
                // then this will no-op
                await client.CloseAsync();

                // Cancel the token
                cancellationTokenSource.Cancel();
            }
        }
    }
}
