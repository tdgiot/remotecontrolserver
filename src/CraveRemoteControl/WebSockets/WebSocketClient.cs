﻿using System;
using System.Diagnostics;
using System.Net.WebSockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CraveRemoteControl.Util;

namespace CraveRemoteControl.WebSockets
{
    public class WebSocketClient : Client
    {
        private System.Net.WebSockets.WebSocket mWebSocket;

        // Wait 250 ms before giving up on a Close
        private static readonly TimeSpan _closeTimeout = TimeSpan.FromMilliseconds(250);

        // 4KB default fragment size (we expect most messages to be very short)
        private const int _receiveLoopBufferSize = 4 * 1024;
        private readonly int? _maxIncomingMessageSize;

        // Queue for sending messages
        private readonly TaskQueue _sendQueue = new TaskQueue();

        internal Task ProcessWebSocketRequestAsync(WebSocket webSocket, CancellationToken disconnectToken)
        {
            if (webSocket == null)
            {
                throw new ArgumentNullException("webSocket");
            }

            var receiveContext = new ReceiveContext(webSocket, disconnectToken, this._maxIncomingMessageSize, _receiveLoopBufferSize);

            return ProcessWebSocketRequestAsync(webSocket, disconnectToken, state =>
            {
                var context = (ReceiveContext)state;

                return WebSocketMessageReader.ReadMessageAsync(context.WebSocket, context.BufferSize, context.MaxIncomingMessageSize, context.DisconnectToken);
            },
            receiveContext);
        }

        internal async Task ProcessWebSocketRequestAsync(WebSocket webSocket, CancellationToken disconnectToken, Func<object, Task<WebSocketMessage>> messageRetriever, object state)
        {
            bool closedReceived = false;

            try
            {
                this.mWebSocket = webSocket;
                if (this.onOpenEventHandler != null)
                {
                    this.onOpenEventHandler(this);
                }

                // dispatch incoming messages
                while (!disconnectToken.IsCancellationRequested && !closedReceived)
                {
                    WebSocketMessage incomingMessage = await messageRetriever(state);
                    switch (incomingMessage.MessageType)
                    {
                        case WebSocketMessageType.Binary:
                            if (this.onBinaryMessageReceivedEventHandler != null)
                            {
                                this.onBinaryMessageReceivedEventHandler(this, (byte[]) incomingMessage.Data);
                            }
                            break;

                        case WebSocketMessageType.Text:
                            if (this.onStringMessageReceivedEventHandler != null)
                            {
                                this.onStringMessageReceivedEventHandler(this, (string) incomingMessage.Data);
                            }
                            break;

                        default:
                            closedReceived = true;

                            // If we received an incoming CLOSE message, we'll queue a CLOSE frame to be sent.
                            // We'll give the queued frame some amount of time to go out on the wire, and if a
                            // timeout occurs we'll give up and abort the connection.
                            await Task.WhenAny(CloseAsync(), Task.Delay(_closeTimeout));
                            break;
                    }
                }

            }
            catch (OperationCanceledException ex)
            {
                // ex.CancellationToken never has the token that was actually cancelled
                if (!disconnectToken.IsCancellationRequested)
                {
                    if (this.onErrorEventHandler != null)
                    {
                        this.onErrorEventHandler(this, ex);
                    }
                }
            }
            catch (ObjectDisposedException)
            {
                // If the websocket was disposed while we were reading then noop
            }
            catch (Exception ex)
            {
                if (IsFatalException(ex))
                {
                    if (this.onErrorEventHandler != null)
                    {
                        this.onErrorEventHandler(this, ex);
                    }
                }
            }
            if (this.onCloseEventHandler != null)
            {
                this.onCloseEventHandler(this);
            }
        }

        public override Task SendMessage(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            return SendAsync(message);
        }

        public override Task SendMessage(byte[] message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            return SendAsync(new ArraySegment<byte>(message), WebSocketMessageType.Binary);
        }

        internal Task SendAsync(string message)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            return SendAsync(new ArraySegment<byte>(buffer), WebSocketMessageType.Text);
        }

        internal virtual Task SendAsync(ArraySegment<byte> message, WebSocketMessageType messageType, bool endOfMessage = true)
        {
            if (GetWebSocketState(this.mWebSocket) != WebSocketState.Open)
            {
                return TaskAsyncHelper.Empty;
            }

            var sendContext = new SendContext(this.mWebSocket, message, messageType, endOfMessage);

            return _sendQueue.Enqueue(async state =>
            {
                var context = (SendContext)state;

                if (GetWebSocketState(context.WebSocket) != WebSocketState.Open)
                {
                    return;
                }

                try
                {
                    await context.WebSocket.SendAsync(context.Message, context.MessageType, context.EndOfMessage, CancellationToken.None);
                }
                catch (Exception ex)
                {
                    // Swallow exceptions on send
                    Trace.TraceError("Error while sending: " + ex);
                }
            },
            sendContext);
        }

        public virtual Task CloseAsync()
        {
            if (IsClosedOrClosedSent(this.mWebSocket))
            {
                return TaskAsyncHelper.Empty;
            }

            var closeContext = new CloseContext(this.mWebSocket);

            return _sendQueue.Enqueue(async state =>
            {
                var context = (CloseContext)state;

                if (IsClosedOrClosedSent(context.WebSocket))
                {
                    return;
                }

                try
                {
                    await context.WebSocket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
                }
                catch (Exception ex)
                {
                    // Swallow exceptions on close
                    Trace.TraceError("Error while closing the websocket: " + ex);
                }
            },
            closeContext);
        }

        private static bool IsFatalException(Exception ex)
        {
            // If this exception is due to the underlying TCP connection going away, treat as a normal close
            // rather than a fatal exception.
            COMException ce = ex as COMException;
            if (ce != null)
            {
                switch ((uint)ce.ErrorCode)
                {
                    // These are the three error codes we've seen in testing which can be caused by the TCP connection going away unexpectedly.
                    case 0x800703e3:
                    case 0x800704cd:
                    case 0x80070026:
                        return false;
                }
            }

            // unknown exception; treat as fatal
            return true;
        }

        private static bool IsClosedOrClosedSent(WebSocket webSocket)
        {
            var webSocketState = GetWebSocketState(webSocket);

            return webSocketState == WebSocketState.Closed ||
                   webSocketState == WebSocketState.CloseSent ||
                   webSocketState == WebSocketState.Aborted;
        }

        private static WebSocketState GetWebSocketState(WebSocket webSocket)
        {
            try
            {
                return webSocket.State;
            }
            catch (ObjectDisposedException)
            {
                return WebSocketState.Closed;
            }
        }

        private class CloseContext
        {
            public WebSocket WebSocket;

            public CloseContext(WebSocket webSocket)
            {
                WebSocket = webSocket;
            }
        }

        private class SendContext
        {
            public WebSocket WebSocket;
            public ArraySegment<byte> Message;
            public WebSocketMessageType MessageType;
            public bool EndOfMessage;

            public SendContext(WebSocket webSocket, ArraySegment<byte> message, WebSocketMessageType messageType, bool endOfMessage)
            {
                WebSocket = webSocket;
                Message = message;
                MessageType = messageType;
                EndOfMessage = endOfMessage;
            }
        }

        private class ReceiveContext
        {
            public WebSocket WebSocket;
            public CancellationToken DisconnectToken;
            public int? MaxIncomingMessageSize;
            public int BufferSize;

            public ReceiveContext(WebSocket webSocket, CancellationToken disconnectToken, int? maxIncomingMessageSize, int bufferSize)
            {
                WebSocket = webSocket;
                DisconnectToken = disconnectToken;
                MaxIncomingMessageSize = maxIncomingMessageSize;
                BufferSize = bufferSize;
            }
        }
    }
}
