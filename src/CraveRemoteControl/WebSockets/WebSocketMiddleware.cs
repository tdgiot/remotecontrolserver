﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;

namespace CraveRemoteControl.WebSockets
{
    public class WebSocketMiddleware<T> : OwinMiddleware where T : WebSocketConnection
    {
        public WebSocketMiddleware(OwinMiddleware next) : base(next)
        {
        }

        public override Task Invoke(IOwinContext context)
        {
            T socketConnection = Activator.CreateInstance<T>();
            return socketConnection.AcceptRequestAsync(context);
        }
    }
}
