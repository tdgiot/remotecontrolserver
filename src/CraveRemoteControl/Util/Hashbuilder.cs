﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CraveRemoteControl.Util
{
    /// <summary>
    /// Class which is used to create hash values.
    /// </summary>
    public class HashBuilder
    {
        #region Fields

        private readonly StringBuilder stringBuilder;
        private readonly BinaryWriter binaryWriter;

        private readonly string salt;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HashBuilder"/> class.
        /// </summary>
        /// <param name="salt">The salt to use when calculating the hash.</param>
        public HashBuilder(string salt)
        {
            if (string.IsNullOrWhiteSpace(salt))
            {
                throw new ApplicationException("Salt is empty!");
            }

            this.salt = salt;
            this.stringBuilder = new StringBuilder();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add one or multiple parameters to this hash builder
        /// </summary>
        /// <param name="param">Objects to add as parameter</param>
        /// <returns>Instance of the this <see cref="HashBuilder"/></returns>
        public HashBuilder Add(params object[] param)
        {
            this.AddParameter(param);

            return this;
        }

        /// <summary>
        /// Adds a parameter to the hash
        /// </summary>
        /// <param name="parameters">The parameters to add.</param>
        private void AddParameter(object[] parameters)
        {
            if (parameters != null)
            {
                foreach (object item in parameters)
                {
                    this.AddParameter(item);
                }
            }
        }

        /// <summary>
        /// Adds a parameter to the hash.
        /// </summary>
        /// <param name="parameter">The parameter to add. If the parameter is an array, it's items will be added separately.</param>
        private void AddParameter(object parameter)
        {
            if (parameter == null)
            {
                return;
            }

            if (this.binaryWriter != null)
            {
                this.binaryWriter.Write(this.GetByteValue(parameter));
            }
            else
            {
                this.stringBuilder.Append(this.GetStringValue(parameter));
            }
        }

        /// <summary>
        /// Gets the hash value.
        /// </summary>
        /// <returns>A <see cref="string"/> instance containing the hash.</returns>
        public string GetHash()
        {
            HashAlgorithm hashAlgorithm = new SHA256Managed();

            byte[] hashedBytes;
            if (this.binaryWriter != null)
            {
                hashedBytes = hashAlgorithm.ComputeHash(this.binaryWriter.BaseStream);
            }
            else
            {
                string text = this.stringBuilder + this.salt;
                byte[] bytes = Encoding.UTF8.GetBytes(text);
                hashedBytes = hashAlgorithm.ComputeHash(bytes);
            }

            return BitConverter.ToString(hashedBytes).Replace("-", string.Empty);
        }

        /// <summary>
        /// Gets the string representation, which is the hash value.
        /// </summary>
        /// <returns>A <see cref="string"/> instance containing the hash.</returns>
        public override string ToString()
        {
            // For debugging purposes, to see what the parameter string is
            return this.stringBuilder.ToString();
        }

        /// <summary>
        /// Gets the string value for a parameter.
        /// </summary>
        /// <param name="parameter">The parameter to get the string value for.</param>
        /// <returns>A <see cref="string"/> instance representing the parameter.</returns>
        private string GetStringValue(object parameter)
        {
            string value;

            if (parameter == null)
            {
                value = string.Empty;
            }
            else if (parameter is double)
            {
                BitConverter.GetBytes((double)parameter);
                value = ((double)parameter).ToString("N", CultureInfo.InvariantCulture);
            }
            else if (parameter is decimal)
            {
                value = ((decimal)parameter).ToString("N", CultureInfo.InvariantCulture);
            }
            else if (parameter is float)
            {
                value = ((float)parameter).ToString("N", CultureInfo.InvariantCulture);
            }
            else if (parameter.GetType().IsPrimitive || parameter is string)
            {
                value = parameter.ToString();
            }
            else if (parameter.GetType().IsEnum)
            {
                value = ((int)parameter).ToString();
            }
            else
            {
                throw new ApplicationException($"Type not supported for hash: '{parameter.GetType()}'");
            }

            return value;
        }

        private byte[] GetByteValue(object parameter)
        {
            byte[] value;

            if (parameter is double)
            {
                value = BitConverter.GetBytes((double)parameter);
            }
            else if (parameter is float)
            {
                value = BitConverter.GetBytes((float)parameter);
            }
            else if (parameter is Stream)
            {
                Stream stream = (Stream)parameter;
                value = new byte[stream.Length];
                stream.Position = 0;
                stream.Read(value, 0, (int)stream.Length);
            }
            else if (parameter.GetType().IsPrimitive || parameter is string)
            {
                string s = parameter.ToString();
                value = Encoding.UTF8.GetBytes(s);
            }
            else if (parameter.GetType().IsEnum)
            {
                value = BitConverter.GetBytes((int)parameter);
            }
            else
            {
                throw new ApplicationException($"Type not supported for hash: '{parameter.GetType()}'");
            }

            return value;
        }

        #endregion
    }
}
