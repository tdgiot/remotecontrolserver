﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CraveRemoteControl.Util
{
    public static class Authorization
    {
        private const int MaxMinutesDifference = 7;
        public const string GenericSalt = "d27392d3aa07a5d07486e7a2e864b29a57509d51244861a68badbb9d76ebaa50";

        public static bool IsAuthorized(NameValueCollection queryParameters)
        {
            if (queryParameters == null)
                return false;

            long timestamp;
            if (queryParameters["timestamp"] == null || !long.TryParse((string)queryParameters["timestamp"], out timestamp))
                return false;

            string identifier = queryParameters["identifier"];
            if (identifier == null)
                return false;

            string hash = queryParameters["hash"];
            if (hash == null)
                return false;

            return Authorization.IsAuthorized(timestamp, identifier, hash);
        }

        public static bool IsAuthorized(long timestamp, string identifier, string hash)
        {
            if (!Authorization.IsValidTimestamp(timestamp))
                return false;

            return GetHash(timestamp, identifier).Equals(hash, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string GetHash(long timestamp, string identifier)
        {
            HashBuilder hashBuilder = new HashBuilder(Authorization.GenericSalt);
            hashBuilder.Add(timestamp);
            hashBuilder.Add(identifier);

            return hashBuilder.GetHash();
        }

        private static bool IsValidTimestamp(long timestamp)
        {
            DateTime requestDateTimeUTC = timestamp.FromUnixTime();
            TimeSpan requestAge = DateTime.UtcNow - requestDateTimeUTC;

            return requestAge.TotalMinutes > -MaxMinutesDifference && requestAge.TotalMinutes < MaxMinutesDifference;
        }

        public static DateTime FromUnixTime(this long unixTime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }
    }
}
