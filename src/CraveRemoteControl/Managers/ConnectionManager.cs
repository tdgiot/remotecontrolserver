﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CraveRemoteControl.WebSockets;
using System.Collections.Concurrent;

namespace CraveRemoteControl.Managers
{
    class ConnectionManager
    {
        private static readonly ConnectionManager instance = new ConnectionManager();

        private readonly ConcurrentDictionary<string, Channel> channels = new ConcurrentDictionary<string, Channel>();
        private readonly ConcurrentDictionary<string, Logcat> logcats = new ConcurrentDictionary<string, Logcat>();

        private ConnectionManager()
        {			
        }

        public static ConnectionManager Instance => ConnectionManager.instance;

        public List<Channel> GetChannels()
        {
            return new List<Channel>(this.channels.Values);
        }

        public void AddConnection(Client client, WebSocketConnectionType connectionType, string identifier)
        {
            switch (connectionType)
            {
                case WebSocketConnectionType.Emenu:
                    this.AddEmenu(client, identifier);
                    break;

                case WebSocketConnectionType.Viewer:
                    this.AddViewer(client, identifier);
                    break;

                case WebSocketConnectionType.WriteLog:
                    this.AddLogWriter(client, identifier);
                    break;

                case WebSocketConnectionType.ReadLog:
                    this.AddLogReader(client, identifier);
                    break;
            }
        }
        

        private void AddEmenu(Client client, string identifier)
        {
            if (!string.IsNullOrWhiteSpace(identifier))
            {
                identifier = FixIdentifier(identifier);

                System.Console.Out.WriteLine("Emenu connected as source for " + identifier);

                this.GetOrCreateChannel(identifier).SetSource(client);
            }
        }

        private void AddViewer(Client client, string identifier)
        {
            System.Console.Out.WriteLine("Viewer connected to " + identifier);

            this.GetOrCreateChannel(identifier).AddViewer(client);
        }  

        public Channel GetOrCreateChannel(string identifier)
        {
            return this.channels.GetOrAdd(identifier, new Channel(identifier));
        }

        private void AddLogWriter(Client client, string identifier)
        {
            if (!string.IsNullOrWhiteSpace(identifier))
            {
                identifier = FixIdentifier(identifier);

                System.Console.Out.WriteLine("Log writer connected for " + identifier);

                this.GetOrCreateLogcat(identifier).SetWriter(client);
            }
        }

        private void AddLogReader(Client client, string identifier)
        {
            System.Console.Out.WriteLine("Log reader connected to " + identifier);

            this.GetOrCreateLogcat(identifier).AddReader(client);
        }

        private Logcat GetOrCreateLogcat(string identifier)
        {
            return this.logcats.GetOrAdd(identifier, new Logcat(identifier));
        }

        private string FixIdentifier(string identifier)
        {
            if (identifier.Length == 17)
            {
                return identifier;
            }

            StringBuilder builder = new StringBuilder();
            string[] parts = identifier.Split(new[] { ':' }, StringSplitOptions.None);
            for (int i = 0; i < parts.Length; i++)
            {
                if (i > 0)
                {
                    builder.Append(":");
                }

                if (parts[i].Length == 1)
                {
                    builder.Append("0");
                }
                builder.Append(parts[i]);
            }
            return builder.ToString();
        }
    }
}
