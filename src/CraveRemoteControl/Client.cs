﻿using System;
using System.Threading.Tasks;

namespace CraveRemoteControl
{
    public abstract class Client
    {
        public String ViewingEmenuIdentifier { get; set; }
        public String EmenuIdentifier { get; set; }

        public delegate void OnOpen(Client client);
        public delegate void OnStringMessageReceived(Client client, string message);
        public delegate void OnBinaryMessageReceived(Client client, byte[] message);
        public delegate void OnError(Client client, Exception exception);
        public delegate void OnClose(Client client);

        public virtual Task SendMessage(string message) { throw new NotImplementedException(); }

        public virtual Task SendMessage(byte[] message) { throw new NotImplementedException(); }

        protected OnOpen onOpenEventHandler;
        public event OnOpen Opened
        {
            add
            {
                this.onOpenEventHandler += value;
            }
            remove
            {
                this.onOpenEventHandler -= value;
            }
        }

        protected OnStringMessageReceived onStringMessageReceivedEventHandler;
        public event OnStringMessageReceived StringMessageReceived
        {
            add
            {
                this.onStringMessageReceivedEventHandler += value;
            }
            remove
            {
                this.onStringMessageReceivedEventHandler -= value;
            }
        }

        protected OnBinaryMessageReceived onBinaryMessageReceivedEventHandler;
        public event OnBinaryMessageReceived BinaryMessageReceived
        {
            add
            {
                this.onBinaryMessageReceivedEventHandler += value;
            }
            remove
            {
                this.onBinaryMessageReceivedEventHandler -= value;
            }
        }

        protected OnError onErrorEventHandler;
        public event OnError Error
        {
            add
            {
                this.onErrorEventHandler += value;
            }
            remove
            {
                this.onErrorEventHandler -= value;
            }
        }

        protected OnClose onCloseEventHandler;
        public event OnClose Closed
        {
            add
            {
                this.onCloseEventHandler += value;
            }
            remove
            {
                this.onCloseEventHandler -= value;
            }
        }
    }
}
