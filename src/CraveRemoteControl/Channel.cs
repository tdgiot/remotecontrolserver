﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using CraveRemoteControl.WebSockets;

namespace CraveRemoteControl
{
    class Channel
    {
        private Client currentSource;
        private readonly List<Client> viewers = new List<Client>();
        private readonly Timer pingTimer = new Timer();

        private readonly object viewerLock = new object();

        public string Identifier { get; private set; }

        public bool Active => this.currentSource != null;

        public Channel(string identifier)
        {
            this.Identifier = identifier;

            this.pingTimer.AutoReset = true;
            this.pingTimer.Interval = 60*1000;
            this.pingTimer.Elapsed += PingTimer_Elapsed;
        }

        private void PingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.currentSource != null)
            {
                this.currentSource.SendMessage("ping");
            }
            else
            {
                pingTimer.Stop();
            }
        }

        public void SetSource(Client emenu)
        {
            if (this.currentSource != null)
            {
                this.CloseSource(this.currentSource);
            }

            this.currentSource = emenu;

            this.pingTimer.Start();

            emenu.Opened += Emenu_Opened;
            emenu.BinaryMessageReceived += Emenu_BinaryMessageReceived;
            emenu.Closed += Emenu_Closed;
        }

        private void Emenu_Opened(Client emenu)
        {
            lock (this.viewers)
            {
                if (this.viewers.Count > 0)
                {
                    emenu.SendMessage("sync");
                }
            }
        }

        private void CloseSource(Client emenu)
        {
            if (emenu == this.currentSource)
            {
                this.currentSource = null;
                this.pingTimer.Stop();
            }

            emenu.BinaryMessageReceived -= Emenu_BinaryMessageReceived;
            emenu.Closed -= Emenu_Closed;
            emenu.Opened -= Emenu_Opened;
        }


        private void Emenu_Closed(Client client)
        {
            this.CloseSource(client);
        }

        private void Emenu_BinaryMessageReceived(Client client, byte[] message)
        {
            lock (this.viewerLock)
            {
                if (this.viewers.Count > 0)
                {
                    foreach (Client viewer in this.viewers)
                    {
                        viewer.SendMessage(message);
                    }
                }
            }
        }

        public void AddViewer(Client viewer)
        {
            lock (this.viewerLock)
            {
                this.viewers.Add(viewer);

                viewer.StringMessageReceived += Viewer_StringMessageReceived;
                viewer.Closed += Viewer_Closed;
            }
            this.currentSource?.SendMessage("sync");
        }

        private void Viewer_StringMessageReceived(Client client, string message)
        {
            if (!message.StartsWith("id:"))
            {
                if (message.StartsWith("close"))
                {
                    ((WebSocketClient) client).CloseAsync();
                }
                else
                {
                    this.currentSource?.SendMessage(message);
                }
            }
        }

        private void Viewer_Closed(Client client)
        {
            lock (this.viewerLock)
            {
                this.viewers.Remove(client);

                client.StringMessageReceived -= Viewer_StringMessageReceived;
                client.Closed -= Viewer_Closed;

                if (this.viewers.Count == 0)
                {
                    this.currentSource?.SendMessage("stop");
                }
            }
        }

        public void StartLogging()
        {
            this.currentSource?.SendMessage("log");
        }

        public int ViewerCount
        {
            get
            {
                lock (this.viewerLock)
                {
                    return this.viewers.Count;
                }   
            }
        }
    }
}