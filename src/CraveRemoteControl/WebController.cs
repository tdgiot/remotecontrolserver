﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using CraveRemoteControl.Managers;
using CraveRemoteControl.Util;

namespace CraveRemoteControl
{
    [AllowAnonymous]
    public class WebController : ApiController
    {
        [HttpGet]
        [Route("test")]
        public HttpResponseMessage Test()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Hello World");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        [HttpGet]
        [Route("control")]
        public HttpResponseMessage Control(long timestamp, string identifier, string hash)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            // By just replacing the 'http'-part with 'ws' it works for both 'ws' (unsecure) and 'wss' (secure) connections.
            string host = Url.Content("~/").Replace("http", "wss");
            if (!host.EndsWith("/"))
            {
                host += "/";
            }

            string url = string.Format("{0}view?timestamp={1}&identifier={2}&hash={3}", host, timestamp, identifier, hash);
            response.Content = new StringContent(viewerHtml.Replace("{url}", url));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        [HttpGet]
        [Route("state")]
        public State State(long timestamp, string identifier, string hash)
        {
            State state = new State();
            if (Authorization.IsAuthorized(timestamp, identifier, hash))
            {
                Channel channel = ConnectionManager.Instance
                    .GetChannels()
                    .FirstOrDefault(x => x != null && x.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase) && x.Active);

                if (channel != null)
                {
                    state.Active = true;
                    state.Viewers = channel.ViewerCount;
                    state.Message = "Client is online";
                }
                else
                {
                    state.Message = "Client not online";
                }
            }
            else
            {
                state.Message = "Authorization failed";
            }
            return state;
        }

        // For dev purposes only
        [HttpGet]
        [Route("list")]
        public HttpResponseMessage List(string password)
        {
            StringBuilder builder = new StringBuilder();
            HttpResponseMessage response = new HttpResponseMessage();

            if (password.Equals("JNdUnqFbdsEtC88eypoI", StringComparison.InvariantCultureIgnoreCase))
            {
                IEnumerable<Channel> channels = ConnectionManager.Instance.GetChannels().Where(x => x != null && x.Active);

                string host = Url.Content("~/");
                if (!host.EndsWith("/"))
                {
                    host += "/";
                }

                long timestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                builder.Append("<table>");
                foreach (Channel channel in channels)
                {
                    string hash = Authorization.GetHash(timestamp, channel.Identifier);

                    string controlUrl = string.Format("{0}control?timestamp={1}&identifier={2}&hash={3}", host, timestamp, channel.Identifier, hash);
                    string logcatUrl = string.Format("{0}read?timestamp={1}&identifier={2}&hash={3}", host, timestamp, channel.Identifier, hash);

                    builder.Append("<tr>");
                    builder.AppendFormat("<td>{0}</td><td><a href=\"{1}\">Control</a></td><td><a href=\"{2}\">Logcat</a></td>", channel.Identifier, controlUrl, logcatUrl);
                    builder.Append("</tr>");
                }
                builder.Append("</table>");
            }
            else
            {
                builder.Append("Access denied");
            }

            response.Content = new StringContent(builder.ToString());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        [HttpGet]
        [Route("read")]
        public HttpResponseMessage Read(long timestamp, string identifier, string hash)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            // By just replacing the 'http'-part with 'ws' it works for both 'ws' (unsecure) and 'wss' (secure) connections.
            string host = Url.Content("~/").Replace("https", "wss");
            if (!host.EndsWith("/"))
            {
                host += "/";
            }

            string url = string.Format("{0}readlog?timestamp={1}&identifier={2}&hash={3}", host, timestamp, identifier, hash);
            response.Content = new StringContent(logReaderHtml.Replace("{url}", url));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        const string viewerHtml =
        @"<!DOCTYPE html>
        <html lang = ""en"" xmlns=""http://www.w3.org/1999/xhtml"">
          <head>
            <meta charset = ""utf-8"" />
          </head>
          <body style=""text-align:center;"">
            <div>
              <canvas id = ""canvas"" width=""1280"" height=""800"" style=""width: 1280px; height: 800px;""></canvas>
            </div>
            <br>
            <br>
            <button type=""button"" onclick=""ws.send('[1, 1000, 0, 0, 0]')"">Power Button</button>
    
	        <script src=""viewer/YUVCanvas.js""></script>
            <script src=""viewer/Decoder.js""></script>
            <script src=""viewer/Player.js""></script>
            <script src=""viewer/Events.js""></script>
	        <script src=""viewer/canvas.js""></script>

            <script>
                startWebSocketClient(""{url}"");
            </script>
          </body>
        </html>";

        const string logReaderHtml =
       @"<!DOCTYPE html>
        <html lang = ""en"" xmlns=""http://www.w3.org/1999/xhtml"">
          <head>
            <meta charset = ""utf-8"" />
          </head>
          <body>            
            <style>
            #log {
                width: 90%;
                height: 500px;
                border: 1px solid #ccc;
	            background-color: #fff;
                padding: 5px;
                font-family: consolas;
                font-size: 10pt;
                overflow: auto;
            }​
            </style>

            <div id=""log"" />
    
	        <script src=""viewer/Log.js""></script>

            <script>
                startWebSocketClient(""{url}"");
            </script>
          </body>
        </html>";
    }
}

