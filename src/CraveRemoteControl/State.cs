﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CraveRemoteControl
{
    public class State
    {
        public bool Active { get; set; }
        public int Viewers { get; set; }
        public string Message { get; set; } = string.Empty;
    }
}
