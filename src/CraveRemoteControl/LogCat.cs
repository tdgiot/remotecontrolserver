﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CraveRemoteControl.Managers;
using CraveRemoteControl.WebSockets;

namespace CraveRemoteControl
{
    class Logcat
    {
        private Client currentWriter;
        private readonly List<Client> readers = new List<Client>();

        private readonly object readerLock = new object();

        public string Identifier { get; private set; }

        public bool Active => this.currentWriter != null;

        public Logcat(string identifier)
        {
            this.Identifier = identifier;
        }

        public void SetWriter(Client writer)
        {
            if (this.currentWriter != null)
            {
                this.CloseWriter(this.currentWriter);
            }

            this.currentWriter = writer;

            writer.Opened += Emenu_Opened;
            writer.StringMessageReceived += Writer_StringMessageReceived;
            writer.Closed += Writer_Closed;
        }

        private void Emenu_Opened(Client writer)
        {
            lock (this.readers)
            {
                if (this.readers.Count > 0)
                {
                    writer.SendMessage("start");
                }
                else
                {
                    writer.SendMessage("stop");
                }
            }
        }

        private void CloseWriter(Client writer)
        {
            if (writer == this.currentWriter)
            {
                this.currentWriter = null;
            }

            writer.StringMessageReceived -= Writer_StringMessageReceived;
            writer.Closed -= Writer_Closed;
            writer.Opened -= Emenu_Opened;
        }


        private void Writer_Closed(Client client)
        {
            this.CloseWriter(client);
        }

        private void Writer_StringMessageReceived(Client client, string message)
        {
            lock (this.readerLock)
            {
                if (this.readers.Count > 0)
                {
                    foreach (Client reader in this.readers)
                    {
                        reader.SendMessage(message);
                    }
                }
                else
                {
                    this.currentWriter.SendMessage("stop");
                }
            }
        }

        public void AddReader(Client reader)
        {
            lock (this.readerLock)
            {
                this.readers.Add(reader);

                reader.Closed += Reader_Closed;
            }

            if (this.currentWriter == null)
            {
                Channel channel = ConnectionManager.Instance.GetOrCreateChannel(this.Identifier);
                channel.StartLogging();
            }
        }

        private void Reader_Closed(Client client)
        {
            lock (this.readerLock)
            {
                this.readers.Remove(client);

                client.Closed -= Reader_Closed;

                if (this.readers.Count == 0)
                {
                    this.currentWriter?.SendMessage("stop");
                }
            }
        }
    }
}