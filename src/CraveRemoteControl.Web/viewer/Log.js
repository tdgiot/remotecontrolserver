var ws;
var log = document.getElementById("log");
var autoScroll = true;

function startWebSocketClient(url) {
	ws = new WebSocket(url);

	ws.onopen = function(event) {
		console.log("connected");
	};

	ws.onmessage = function (event) {
	    if (event.data.includes('WifiHW') || event.data.includes('wpa_supplicant')) {
	        return;
	    }

	    log.innerHTML += event.data + '<br/>';
        
        if (autoScroll) {
            log.scrollTop = log.scrollHeight;
        }
	};

	ws.onclose = function() {
		console.log("disconnected");
	};
};

document.getElementById('log').addEventListener('wheel', function (e) {
    if (e.deltaY < 0) {
        autoScroll = false;
    }
    if (e.deltaY > 0) {
        autoScroll = true;
    }
});