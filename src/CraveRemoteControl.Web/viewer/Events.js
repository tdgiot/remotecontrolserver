var mouseDown = 0;
var newX = 0;
var newY = 0;

function getcords(event){
	var rect = canvas.getBoundingClientRect();
	var x = event.clientX - rect.left;
	var y = event.clientY - rect.top;
	return [x, y];
}

function supress(e){
	e.cancelBubble = true;
	
	if( e.stopPropagation ) 
		e.stopPropagation();
		
	e.preventDefault();
	return false;
}

function doKeyDown(e){
	sendKey(1,e.keyCode,e.altKey,e.shiftKey,e.ctrlKey);
	return supress(e);
}

function doKeyUp(e){
	sendKey(0,e.keyCode,e.altKey,e.shiftKey,e.ctrlKey);
	return supress(e);
}

function sendKey(down,key,alt,shift,ctrl){
	ws.send(JSON.stringify([down,key != 224 ? key : 91,alt ? 1 : 0,shift ? 1 : 0,ctrl ? 1 : 0]));
}

function doMouseDown(e){
	var type = (e.button == 2 ? 4 : 1);
	mouseDown = type;
	sendMouse(type, e);
	canvas.focus();
	return supress(e);
}

function doMouseUp(e){
	var type = (e.button == 2 ? 3 : 0);
	mouseDown = type;
	sendMouse(type, e);
	return supress(e);
}

function doMouseMove(e){
	if(mouseDown != 1)
		return;
	sendMouse(2, e);
}

function doMouseLeave(e){
	if(mouseDown == 1 || mouseDown == 4){
		e.target.onmousemove = null;
		mouseDown = (mouseDown == 1 ? 0 : 3);
		sendMouseWS(0,newX,newY);
	}
}

function sendMouse(down, e){
	var cords = getcords(e);
	newX = cords[0] / canvas.width;
	newY = cords[1] / canvas.height;
	sendMouseWS(down,newX,newY);
}

function sendMouseWS(down, x, y){
	var cords = [x,y];
	ws.send(JSON.stringify([down,cords[0],cords[1]]));
}