var ws;

var player = new Player({
    useWorker: true,
    workerFile: "viewer/Decoder.js"
});

var canvas = player.canvas;
var old_canvas = document.getElementById('canvas');
old_canvas.parentNode.replaceChild(canvas, old_canvas);

canvas.width = 1280;
canvas.height = 800;
canvas.tabIndex = 1000;
canvas.addEventListener("mousedown", doMouseDown, false);
canvas.addEventListener("mouseup", doMouseUp, false);
canvas.addEventListener("mousemove", doMouseMove, false);
canvas.addEventListener("keydown", doKeyDown, false);
canvas.addEventListener("keyup", doKeyUp, false);
canvas.addEventListener("mouseleave", doMouseLeave, false);
canvas.contenteditable = true;

function startWebSocketClient(url) {
    ws = new WebSocket(url);
    ws.binaryType = "arraybuffer";

    ws.onopen = function (event) {
        console.log("connected");
    };

    ws.onmessage = function (event) {
        var array = new Uint8Array(event.data);
        player.decode(array);

        array = null;
        event.data = null;
    };

    ws.onclose = function () {
        console.log("disconnected");
    };
};