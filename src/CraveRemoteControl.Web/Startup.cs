﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using CraveRemoteControl.WebSockets;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;

[assembly: OwinStartup(typeof(CraveRemoteControl.Web.Startup))]

namespace CraveRemoteControl.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            app.Map("/testing", config =>
                                {
                                    config.Run(context =>
                                               {
                                                   using (StreamWriter writer = new StreamWriter(context.Response.Body))
                                                   {
                                                       writer.Write("Testing: {0}", DateTime.UtcNow);
                                                   }
                                                   return Task.FromResult(0);
                                               });
                                });

            app.Map("/emenu", config =>
                              {
                                  config.UseCors(CorsOptions.AllowAll).Use<WebSocketMiddleware<WebSocketConnection>>();
                              });
            app.Map("/view", config =>
                             {
                                 config.UseCors(CorsOptions.AllowAll).Use<WebSocketMiddleware<WebSocketConnection>>();
                             });
            app.Map("/writelog", config =>
                                 {
                                     config.UseCors(CorsOptions.AllowAll).Use<WebSocketMiddleware<WebSocketConnection>>();
                                 });
            app.Map("/readlog", config =>
                                {
                                    config.UseCors(CorsOptions.AllowAll).Use<WebSocketMiddleware<WebSocketConnection>>();
                                });
            
            HttpConfiguration httpConfiguration = new HttpConfiguration();
            httpConfiguration.MapHttpAttributeRoutes();
            httpConfiguration.EnsureInitialized();
            httpConfiguration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            app.UseWebApi(httpConfiguration);

            app.UseStaticFiles(new StaticFileOptions
                               {
                                   FileSystem = new PhysicalFileSystem(Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "viewer")),
                                   RequestPath = new PathString("/viewer"),
                                   ServeUnknownFileTypes = true
                               });

            app.Map(string.Empty, map => map.Run(context => context.Response.WriteAsync("Hello World")));
        }
    }
}
