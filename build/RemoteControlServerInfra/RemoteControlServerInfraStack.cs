﻿using System.Collections.Generic;
using Amazon.CDK;
using Amazon.CDK.AWS.ElasticBeanstalk;
using Crave.Infrastructure;
using Crave.Infrastructure.Models;
using Crave.Infrastructure.CDK.Constructs;
using Crave.Infrastructure.CDK.Extensions;
using Constructs;

namespace RemoteControlServerInfra
{
    public class RemoteControlServerInfraStack : Stack
    {
        private const string RemoteControlServerApplicationName = "RemoteControlServer";

        internal RemoteControlServerInfraStack(Construct scope, string id, CraveEnvironment craveEnvironment, IStackProps? props = null) : base(scope, id, props)
        {
            ElasticBeanstalk.ElasticBeanstalkProps envSpecificProps = craveEnvironment.Environment != Crave.Infrastructure.Enums.EnvironmentType.Production
                ? DefaultProperties
                : ProductionProperties;

            ElasticBeanstalk.ElasticBeanstalkProps beanstalkProperties = new ElasticBeanstalk.ElasticBeanstalkProps
            {
                ApplicationName = RemoteControlServerApplicationName,
                SolutionStackName = "64bit Windows Server 2019 v2.10.3 running IIS 10.0",
                AutoScalingMinSize = envSpecificProps.AutoScalingMinSize,
                AutoScalingMaxSize = envSpecificProps.AutoScalingMaxSize,
                HealthEndpoint = "/health",
                InstanceTypes = envSpecificProps.InstanceTypes,
                CnamePrefix = $"{RemoteControlServerApplicationName}-{craveEnvironment.Environment}".ToLower(),
                Type = ElasticBeanstalk.ElasticBeanstalkType.WebTier,
                PublishedArtifactsFolder = "artifacts/CraveRemoteControl.Web.zip",
                SslCertificateArn = CraveCertificates.GetCraveCloudXyzCertificateArn(craveEnvironment.Environment),
                AdditionalConfigurationOptions = new List<CfnEnvironment.OptionSettingProperty>
                {
                    CreateSettingProperty("aws:elasticbeanstalk:healthreporting:system", "SystemType", "enhanced"),
                    CreateSettingProperty("aws:elasticbeanstalk:xray", "XRayEnabled", "true"),
                    CreateSettingProperty("aws:elasticbeanstalk:application:environment", "ASPNETCORE_ENVIRONMENT", craveEnvironment.Environment.ToString())
                }
            };

            _ = new ElasticBeanstalk(this, "remote-control", beanstalkProperties)
                .WithARecord("remotecontrol", craveEnvironment.DomainNames.External);
        }

        private ElasticBeanstalk.ElasticBeanstalkProps DefaultProperties => new ElasticBeanstalk.ElasticBeanstalkProps
        {
            AutoScalingMinSize = 1,
            AutoScalingMaxSize = 1,
            InstanceTypes = "t3.small"
        };

        private ElasticBeanstalk.ElasticBeanstalkProps ProductionProperties => new ElasticBeanstalk.ElasticBeanstalkProps
        {
            AutoScalingMinSize = 1,
            AutoScalingMaxSize = 1,
            InstanceTypes = "t3.medium"
        };

        private static CfnEnvironment.OptionSettingProperty CreateSettingProperty(string optionNamespace, string optionName, string value)
        {
            return new CfnEnvironment.OptionSettingProperty
            {
                Namespace = optionNamespace,
                OptionName = optionName,
                Value = value
            };
        }
    }
}
