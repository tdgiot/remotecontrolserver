﻿using System;
using Amazon.CDK;
using Crave.Infrastructure;
using Crave.Infrastructure.CDK.Extensions;
using Crave.Infrastructure.Models;
using Environment = Amazon.CDK.Environment;

namespace RemoteControlServerInfra
{
    internal sealed class Program
    {
        public static void Main()
        {
            App app = new App();

            CraveEnvironment craveEnvironment = app.GetCraveEnvironment();

            Console.WriteLine("Deploy Environment: {0}", craveEnvironment.Environment);

            Environment deployEnvironment = new Environment
            {
                Account = craveEnvironment.AccountId,
                Region = craveEnvironment.Region
            };

            RemoteControlServerInfraStack stack = new RemoteControlServerInfraStack(app, "RemoteControlServerInfraStack", craveEnvironment, new StackProps
            {
                Env = deployEnvironment
            });

            Tags.Of(stack).Add(CraveTags.ProjectId, "remote-control-server");
            Tags.Of(stack).Add(CraveTags.EnvironmentType, craveEnvironment.Environment.ToString());

            app.Synth();
        }
    }
}